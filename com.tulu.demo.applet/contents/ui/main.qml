import QtQuick 2.0
import QtQuick.Layouts 1.0
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 3.0 as PlasmaComponents3

Item{
    id: root
    property string icon: Qt.resolvedUrl("../image/intel.svg")
    property bool nvidia: false
    property string bashPath: Qt.resolvedUrl("switch.sh").replace("file://", "")
    property string textPath: Qt.resolvedUrl("prime.txt").replace("file://", "")
    property string gpuActivated: "Intel"

    Plasmoid.icon: root.icon

    Component.onCompleted:{
        check()
    }

    PlasmaCore.DataSource{
        id: execdata
        engine: "executable"
        connectedSources: []
        onNewData: {
            var stdout = data["stdout"]
            exited(sourceName, stdout)
            disconnectSource(sourceName)
        }

        function exec(cmd){
            connectSource(cmd)
        }

        signal exited(string cmd, string stdout)
    }

    Connections{
        target: execdata
        onExited:{
            var formattedText = stdout.trim()
            switch(formattedText){
                case "on":
                    root.icon = Qt.resolvedUrl("../image/nvidia.svg")
                    root.gpuActivated = "Nvidia"
                    break;

                case "off":
                    root.icon = Qt.resolvedUrl("../image/intel.svg")
                    root.gpuActivated = "Intel"
                    break;
            }

        }
    }

    function check(){
        execdata.exec("cat " + textPath)
    }

    PlasmaCore.DataSource{
        id: "exec"
        engine: "executable"
        connectedSources: []
        onNewData: disconnectSource(sourceName)

        function exec(cmd){
            connectSource(cmd)
        }
    }

    function switchGPU(){
        if (root.gpuActivated == "Intel"){
            exec.exec("bash " + root.bashPath + " nvidia")
        }
        else{
            exec.exec("bash " + root.bashPath + " intel")
        }
    }

    Plasmoid.preferredRepresentation: Plasmoid.compactRepresentation
    Plasmoid.compactRepresentation: PlasmaCore.IconItem{
        source: root.icon
        active: compactMouse.containsMouse

        MouseArea{
            id: compactMouse
            anchors.fill: parent
            hoverEnabled: true
            onClicked: {
                plasmoid.expanded = !plasmoid.expanded
            }
        }
    }

    Plasmoid.fullRepresentation: Item{
        ColumnLayout {
            anchors.centerIn: parent

            PlasmaComponents3.Button{
                text: 'Switch'
                onClicked: switchGPU()
            }
            PlasmaComponents3.Label{
                id: gpuLabel
                text: root.gpuActivated
            }
        }
    }
}
